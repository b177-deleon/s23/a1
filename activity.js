db.rooms.insertOne({
	name: "Single",
	accommodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	roomsAvailable: 10,
	isAvailable: false
})

db.rooms.insertMany([
	{
		name: "Double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		roomsAvailable: 5,
		isAvailable: false
	},
	{
		name: "Double",
		accommodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		roomsAvailable: 5,
		isAvailable: false
	}
])

db.rooms.insertMany([
	{
		name: "Queen",
		accommodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		roomsAvailable: 15,
		isAvailable: false
	}.
	{
		name: "Queen",
		accommodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		roomsAvailable: 15,
		isAvailable: false
	}
])

db.rooms.find({name: "Double"})

db.rooms.updateOne(
	{name: "Queen"},
	{
		$set: {
			roomsAvailable: 0,
		}

	}
)

db.rooms.deleteMany({
	roomsAvailable: 0
})